import React, { useState } from "react";
import { styled } from "@mui/system";

const grey = {
  50: "#F3F6F9",
  100: "#E7EBF0",
  200: "#E0E3E7",
  300: "#CDD2D7",
  400: "#B2BAC2",
  500: "#A0AAB4",
  600: "#6F7E8C",
  700: "#3E5060",
  800: "#2D3843",
  900: "#1A2027",
};

const Root = styled("div")(
  ({ theme }) => `
  table {
    font-family: IBM Plex Sans, sans-serif;
    font-size: 0.875rem;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid ${theme.palette.mode === "dark" ? grey[800] : grey[200]};
    text-align: left;
    padding: 6px;
  }

  th {
    background-color: ${theme.palette.mode === "dark" ? grey[900] : grey[100]};
  }
  `
);
const INITIAL_STATE = [
  {
    no: "",
    itemCode: "",
    itemDescription: "",
    locationType: "",
    locationCode: "",
    locationDesc: "",
    activityCode: "",
    activityDesc: "",
  },
  {
    no: "",
    itemCode: "",
    itemDescription: "",
    locationType: "",
    locationCode: "",
    locationDesc: "",
    activityCode: "",
    activityDesc: "",
  },
  {
    no: "",
    itemCode: "",
    itemDescription: "",
    locationType: "",
    locationCode: "",
    locationDesc: "",
    activityCode: "",
    activityDesc: "",
  },
  {
    no: "",
    itemCode: "",
    itemDescription: "",
    locationType: "",
    locationCode: "",
    locationDesc: "",
    activityCode: "",
    activityDesc: "",
  },
  {
    no: "",
    itemCode: "",
    itemDescription: "",
    locationType: "",
    locationCode: "",
    locationDesc: "",
    activityCode: "",
    activityDesc: "",
  }
];

export default function UnstyledTable() {
  const [array, setArray] = useState(INITIAL_STATE);
  const [data, setData] = useState(null);
  const addData = (event) => {
    let newData = {
      no: "",
      itemCode: "",
      itemDescription: "",
      locationType: "",
      locationCode: "",
      locationDesc: "",
      activityCode: "",
      activityDesc: "",
    };
    setArray([...array, newData]);
    if (event.key === "Enter") {
      setArray([...array, newData]);
    }
  };

  const updateData = (field, index, event) => {
    let temp = null;
    for (let i = 0; i < array.length; i++) {
      if (array[i] === array[index]) {
        temp = array[i];
      }
    }

    temp[field] = event.target.value;
    setData(temp);
  };

  const submit = () => {
    let submitedData = []
    for (let i=0; i<array.length;i++) {
      if(
        array[i].no && 
        array[i].itemCode && 
        array[i].itemDescription && 
        array[i].locationType && 
        array[i].locationCode && 
        array[i].locationDesc && 
        array[i].activityCode && 
        array[i].activityDesc) {
          submitedData.push(array[i])
      }
    }
    console.log(submitedData, "array");
  };

  return (
    <Root
      sx={{
        width: 500,
        marginLeft: "200px",
        marginTop: "100px",
      }}
    >
      <button onClick={addData}>Tambah Data</button>
      <table aria-label="custom pagination table">
        <thead>
          <tr>
            <th style={{ minWidth: "120px" }}>No</th>
            <th style={{ minWidth: "120px" }}>Item Code</th>
            <th style={{ minWidth: "120px" }}>Item Description</th>
            <th style={{ minWidth: "120px" }}>Location Type</th>
            <th style={{ minWidth: "120px" }}>Location Code</th>
            <th style={{ minWidth: "120px" }}>Location Description</th>
            <th style={{ minWidth: "120px" }}>Activity Code</th>
            <th style={{ minWidth: "120px" }}>Activity Description</th>
          </tr>
        </thead>
        <tbody>
          {array.map((row, i) => (
            <tr key={i}>
              <td>
                <input
                  type="text"
                  onChange={(e) => updateData("no", i, e)}
                  style={{ minWidth: "120px" }}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      submit();
                    }
                  }}
                ></input>
              </td>
              <td>
                <input
                  type="text"
                  onChange={(e) => updateData("itemCode", i, e)}
                  style={{ minWidth: "120px" }}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      submit();
                    }
                  }}
                ></input>
              </td>
              <td>
                <input
                  type="text"
                  onChange={(e) => updateData("itemDescription", i, e)}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      submit();
                    }
                  }}
                  style={{ minWidth: "120px" }}
                ></input>
              </td>
              <td>
                <input
                  type="text"
                  onChange={(e) => updateData("locationType", i, e)}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      submit();
                    }
                  }}
                ></input>
              </td>
              <td>
                <input
                  type="text"
                  onChange={(e) => updateData("locationCode", i, e)}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      submit();
                    }
                  }}
                  style={{ minWidth: "120px" }}
                ></input>
              </td>
              <td>
                <input
                  type="text"
                  onChange={(e) => updateData("locationDesc", i, e)}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      submit();
                    }
                  }}
                  style={{ minWidth: "120px" }}
                ></input>
              </td>
              <td>
                <input
                  type="text"
                  onChange={(e) => updateData("activityCode", i, e)}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      submit();
                    }
                  }}
                  style={{ minWidth: "120px" }}
                ></input>
              </td>
              <td>
                <input
                  type="text"
                  onChange={(e) => updateData("activityDesc", i, e)}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      submit();
                    }
                  }}
                  style={{ minWidth: "120px" }}
                ></input>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Root>
  );
}
